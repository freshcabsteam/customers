<?php
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Material Design for Bootstrap fonts and icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">

    <!-- Material Design for Bootstrap CSS -->
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">
      <link rel="stylesheet" href="css/datepicker.css" >
      <link rel="stylesheet" href="css/style.css" />
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgplY8XAAbxMZ1ZNB8YOb6KjfsUbjG_GY&amp;libraries=places,geometry"></script>
	
    <title>Reserveer uw pick-up</title>
  </head>
  <body>
    <div class="container">
		<div class="row row-header">
			<div class="col text-center">
				<img src="img/tesla.png" />
			</div>
			<div class="col text-center">
				<img src="img/freshcabs.png" />
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div id="spinnerContainer" style="display:none">
					<div class="spinner">
					  <div class="spinner-wrapper">
						<div class="rotator">
						  <div class="inner-spin"></div>
						  <div class="inner-spin"></div>
						</div>
					  </div>
					</div>
                    <div id="spinMessages">
                    </div>
				</div>
				<div class="card" id="formContainer">
					<div class="card-body">
						<section id="step1" class="book-step" style="display:block">
							<form id="contactDetailsForm">
							<div class="container-fluid">
								<div class="row">
									<div class="col">
										<p>Speciaal voor Tesla eigenaren die hun nieuwe Tesla komen ophalen heeft Freshcabs gereduceerde tarieven samengesteld voor het vervoer naar de Tesla Factory in Tilburg. Voor deze gereduceerde tarieven zijn enkele voorwaarden: </p>
										<ul>
											<li>U gaat naar de Factory om uw nieuwe of tweede hands Tesla op te halen;</li>
											<li>U krijgt dit gereduceerde tarief voor maximaal 4 personen;</li>
											<li>Freshcabs mag u met andere klanten die naar de Tesla Factory toe moeten samenvoegen;</li>
										</ul>
                                        <p>Mocht u naar aanleiding van bovenstaande toch nog vragen hebben of bijzondere wensen, dan kunt u altijd contact opnemen met Freshcabs op het telefoonnummer <a href="tel:0031132032290">+31132032290</a>.</p>
                                        <p>Vanuit onderstaande 3 plekken is het vaste tarief EUR 32,50:</p>
                                        <ul>
                                            <li>Tilburg Centraal Station</li>
                                            <li>Schadeherstel Van Gestel</li>
                                            <li>Haans autoschade</li>
                                        </ul>
										<p>Mocht u opgehaald willen worden op een andere locatie dan de drie locaties zoals hierboven genoemd, is dat mogelijk, maar dan tegen een ander tarief. Dit tarief zal  door het systeem berekend worden. Voor ritten buiten Tilburg heeft Freshcabs speciaal voor Tesla klanten een korting van 20% waarbij ritten die verder zijn dan 100KM, u een korting krijgt van 30%.</p>
										<p>Om deze service zo vlekkeloos mogelijk te laten verlopen hebben wij een aantal gegevens van u nodig:</p>
									</div>
								</div>
							
								<div class="row">
									<div class="col">
										<h5>Contactinformatie</h5>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-6">
										<div class="form-group">
											<label for="inputFirstname" class="bmd-label-floating">Voornaam(*)</label>
											<input type="text" class="form-control" id="inputFirstname" required>
										</div>
									</div>
									<div class="col-sm-12 col-md-6">
										<div class="form-group">
											<label for="inputLastname" class="bmd-label-floating">Achternaam (*)</label>
											<input type="text" class="form-control" id="inputLastname" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-6">
										<div class="form-group">
											<label for="inputRN" class="bmd-label-floating">RN Nummer (*)</label>
											<input type="number" class="form-control" id="inputRN" required>
											<span class="bmd-help">Dit is het ordernummer dat Tesla u heeft verstrekt</span>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-6">
										<div class="form-group">
											<label for="inputMobilephone" class="bmd-label-floating">Mobiel telefoonnummer (*)</label>
											<input type="tel" class="form-control" id="inputMobilephone" required>
											<span class="bmd-help">We delen uw telefoonnummer niet, maar hebben dit wel nodig om u te kunnen bereiken</span>
										</div>
									</div>
									<div class="col-sm-12 col-md-6">
										<div class="form-group">
											<label for="inputAlternativephone" class="bmd-label-floating">Alternatief telefoonnummer</label>
											<input type="tel" class="form-control" id="inputAlternativephone">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label for="inputEmail" class="bmd-label-floating">E-mailadres (*)</label>
											<input type="email" class="form-control" id="inputEmail" required>
											<span class="bmd-help">We delen uw email adres niet. We sturen wel een bevestiging van de reservering!</span>
										</div>
									</div>
								</div>
								<div class="row row-actions">
									<div class="col text-right">
										<button type="submit" class="btn btn-primary">Volgende</button>
									</div>
								</div>
							</div>	
							</form>
						</section>
						<section id="step2" class="book-step">
							<form id="pickupForm">
							<div class="container-fluid">
								<div class="row">
									<div class="col-sm-12 col-md-6">
										<p><label class="bmd-label-static">Waar wilt u opgehaald worden?</label></p>
										<div class="radio">
											<label>
												<input type="radio" name="pickupRadios" id="optionsPickup0" value="0">
												Tilburg Centraal Station
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" name="pickupRadios" id="optionsPickup1" value="1">
												Schadenet Van Gestel (Tilburg)
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" name="pickupRadios" id="optionsPickup2" value="2">
												Haans Autoschade (Tilburg)
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" name="pickupRadios" id="optionsPickup3" value="3">
												Ander adres
											</label>
										</div>
										<div id="addressInputContainer" class="form-group" style="display:none">
											<input type="text" class="form-control" id="inputAddress" disabled required>
										</div>
									</div>
									
									<div class="col-sm-12 col-md-6">
										<div class="form-group">
											<label for="inputDateTime" class="bmd-label-static">Wanneer wil je aankomen?</label>
                                            <input type="datetime-local" class="form-control" id="inputDate" value="" min="2018-01-01">
											<span class="bmd-help">dd-mm-jjjj, uu:mm</span>
										</div>
										<div class="form-group">
											<label for="inputNumPassengers" class="bmd-label-static">Met hoeveel personen?</label>
											<select id="inputNumPassengers" class="form-control">
												<option value="1" selected>1 Persoon</option>
												<option value="2">2 Personen</option>
												<option value="3">3 Personen</option>
												<option value="4">4 Personen</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row row-actions">
									<div class="col text-right">
										<button type="button" class="btn btn-default" onClick="gotoStep(1)">Vorige</button>
										<button type="submit" class="btn btn-primary">Controleren</button>
									</div>
								</div>
							</div>
							</form>
						</section>
						<section id="step3" class="book-step">
							<div class="container-fluid">
								<div class="row">
									<div class="col"><h5>Dat is alles! Controleert u de onderstaande gegevens en klik op "Verstuur" om de reservering te plaatsen</h5></div>
								</div>
								<div class="row">
									<div class="col-md-6 col-sm-12">
										<div class="p-2 mb-2 bg-light text-dark">
                                            <span class="text-muted">Uw naam:</span><br>
                                            <span class="text-primary" id="lblName"></span>
                                        </div>
									</div>
									<div class="col-md-6 col-sm-12">
										<div class="p-2 mb-2 bg-light text-dark"><span class="text-muted">Indien nodig kunnen we u bereiken op:</span><br><span class="text-primary" id="lblMobile"></span><span id="lblAlternative"></span></div>
									</div>
									<div class="col-md-6 col-sm-12">
										<div class="p-2 mb-2 bg-light text-dark"><span class="text-muted">We halen u op op:</span><br><span class="text-primary" id="lblPickup"></span>, en brengen u naar de Factory</div>
									</div>
									<div class="col-md-6 col-sm-12">
										<div class="p-2 mb-2 bg-light text-dark"><span class="text-muted">U wordt opgehaald op:</span><br><span class="text-primary" id="lblTimerequest"></span></div>
									</div>
									<div class="col-md-6 col-sm-12">
										<div class="p-2 mb-2 bg-light text-dark"><span class="text-muted">Kort na het versturen van dit formulier bevestigen wij de pick-up op: </span><br><span id="lblEmail" class="text-primary"></span></div>
									</div>
									<div class="col-md-6 col-sm-12">
										<div class="p-2 mb-2 bg-light text-dark"><span class="text-muted">Uw RN-nummer:</span><br><span class="text-primary" id="lblRN"></span></div>
									</div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="p-2 mb-2 bg-light text-dark">
                                            <span class="text-muted">Prijs voor deze rit:</span><br>
                                            <span class="text-primary pricing-table" id="lblPsubtotal"></span><br>
                                            <span class="text-primary pricing-table" id="lblPdiscount" style="border-bottom: 1px solid #999999;"></span><br>
                                            <span class="text-primary pricing-table" id="lblPgtotal"></span>

                                            <input type="hidden" value="" name="price" id="inputPrice"/>
                                            <input type="hidden" value="" name="duration" id="inputDuration"/>
                                            <input type="hidden" value="" name="duration" id="inputDistance"/>
                                        </div>
                                    </div>
									<div class="col-sm-6 col-md-6">
										<div class="form-group">
											<label for="remarksTextarea" class="bmd-label-floating">Moeten wij nog iets weten?</label>
											<textarea class="form-control" id="remarksTextarea" rows="9" maxlength="350"></textarea>
											<span class="bmd-help text-right"><span id="remarksCharCount">0</span> / 350</span>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col"><p>We bewaren uw gegevens zorgvuldig en gebruiken deze enkel om de pick-up uit te voeren. We delen de nodige informatie met Tesla, en niemand anders. We gebruiken de data niet voor marketingdoeleinden. Lees het hele verhaal in onze <a href="#" target="_blank">Privacy Policy</a>.</p></div>
								</div>
								<div class="row row-actions">
									<div class="col text-right">
										<button type="button" class="btn btn-default" onClick="gotoStep(2)">Vorige</button>
										<button id="btnSaveForm" type="button" class="btn btn-success">Verstuur</button>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js" integrity="sha256-CutOzxCRucUsn6C6TcEYsauvvYilEniTXldPa6/wu0k=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/locale/nl.js" integrity="sha256-OxqaUw68tsGCQpee6nrF7sttBmUXE9t3OtiVr8thTd4=" crossorigin="anonymous"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/engine.js"></script>
    <script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
  </body>
</html>