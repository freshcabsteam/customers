$(function(){
    var pickupRadio = -1;
    var pickupOptions = ["Tilburg Centraal Station", "Schadenet Van Gestel (Tilburg)", "Haans Autoschade (Tilburg)"];
    var pickupAddress = '';
    var timerequest;
    var lat = 0;
    var lng = 0;
    var name = '';
    var address = '';
    var street_number = '';
    var city = '';
    var postal = '';
    var country = '';
    var distance = 0;
    var duration = 0;

    updateDateInputs();

    $("#contactDetailsForm").submit(function(e){
        e.preventDefault();
        $("#step1").fadeOut('fast', function(){
            $("#step2").fadeIn('fast');
        });
    });

    $("#pickupForm").submit(function(e){
        e.preventDefault();

        var pickupRadio = $(':radio[name="pickupRadios"]').filter(':checked').val();
        if(typeof pickupRadio === 'undefined'){
            alert('Selecteer alstublieft een ophaal locatie');
            return false;
        }

        $("#step2").fadeOut('fast', function(){
            $("#lblName").text($('#inputFirstname').val() + ' ' + $("#inputLastname").val());
            $("#lblMobile").text(convertPhone($('#inputMobilephone').val(), true));
            if($("#inputAlternativephone").val() && $("#inputAlternativephone").val() !== '') $("#lblAlternative").html(', of <span class="text-primary">'+convertPhone($("#inputAlternativephone").val(), true) +'</span>');
            $("#lblPickup").text(pickupAddress);
            $("#lblEmail").text($("#inputEmail").val());
            $("#lblRN").text($("#inputRN").val());

            timerequest = moment($("#inputDate").val()).subtract(30, 'minutes');
            console.log($("#inputDate").val());
            console.log(timerequest);

            if(pickupRadio < 3){
                $("#lblTimerequest").text(timerequest.format('L, LT'));
            }
            else{
                timerequest = moment($("#inputDate").val()).subtract(parseInt($("#inputDuration").val()), 's');
                $("#lblTimerequest").text(timerequest.format('L, LT'));
            }

            $("#step3").fadeIn('fast');
        });
    });

    $(':radio[name="pickupRadios"]').change(function() {
        pickupRadio = $(this).filter(':checked').val();
        if(pickupRadio == 0){   //Tilburg centraal station
            lat = 51.5605706;
            lng = 5.0811999;
            $("#inputDuration").val('14085');
            $("#inputDistance").val('1319');
            $("#inputPrice").val('32,50');
            $('#lblPgtotal').html('<label>Totaal : </label> € 32,50');
        }
        if(pickupRadio == 1){   //Schadeherstel van Gestel
            lat = 51.5842202;
            lng = 5.0687897;
            $("#inputDuration").val('9304');
            $("#inputDistance").val('648');
            $("#inputPrice").val('32,50');
            $('#lblPgtotal').html('<label>Totaal : </label> € 32,50');
        }
        if(pickupRadio == 2){   //Haans Autoschade
            lat = 51.5665695;
            lng = 5.1104631;
            $("#inputDuration").val('13422');
            $("#inputDistance").val('871');
            $("#inputPrice").val('32,50');
            $('#lblPgtotal').html('<label>Totaal : </label> € 32,50');
        }
        if(pickupRadio == 3){
            $("#addressInputContainer").slideDown('fast');
            $("#inputAddress").prop('disabled', false);
        }
        else{
            $("#addressInputContainer").hide();
            $("#inputAddress").prop('disabled', true);
        }
        if(pickupRadio == 4){
            $("#outsideTilburgAlert").slideDown('fast');
            pickupAddress = 'Buiten Tilburg';
        }
        else{
            $("#outsideTilburgAlert").hide();
            pickupAddress = pickupOptions[pickupRadio];
        }
    });

    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputAddress'));

    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("Onvoldoende informatie gevonden over: '" + place.name + "'");
            return;
        }

        if(jQuery.inArray("establishment", place.types) !== -1 || jQuery.inArray('point_of_interest', place.types) !== -1) name = place.name;

        $.each(place.address_components, function(i, c){
            if(jQuery.inArray("street_number", c.types) !== -1) street_number = c.short_name;
            if(jQuery.inArray("route", c.types) !== -1) address = c.short_name;
            if(jQuery.inArray("locality", c.types) !== -1) city = c.short_name;
            if(jQuery.inArray("country", c.types) !== -1) country = c.short_name;
            if(jQuery.inArray("postal_code", c.types) !== -1) postal = c.short_name;

        });

        lat = place.geometry.location.lat();
        lng = place.geometry.location.lng();

        calculateRoutes(lat, lng);

        pickupAddress = address+' '+street_number+' '+city;
    });

    $('textarea').on("load propertychange keyup input paste",
        function () {

            $("#remarksCharCount").text($(this).val().length);
        });


    $("#btnSaveForm").click(function(){
        $("#formContainer").fadeOut('normal', function(){
            $("#spinnerContainer").fadeIn('fast');
            $.post('/api/v2/rides/issue', {
                pickupRadio: pickupRadio,
                lat: lat,
                lng: lng,
                street: address,
                number: street_number,
                postal: postal,
                city: city,
                country: 'nl',
                numPassengers: $("#inputNumPassengers").val(),
                timerequest: timerequest.toISOString(),
                firstname: $('#inputFirstname').val(),
                name: $('#inputFirstname').val() + ' ' + $("#inputLastname").val(),
                phone: convertPhone($('#inputMobilephone').val(), false),
                alternativePhone: convertPhone($('#inputAlternativephone').val(), false),
                price: $('#inputPrice').val(),
                email: $("#inputEmail").val(),
                remarks: $("#remarksTextarea").val(),
                extras: {
                'rn-number': $("#inputRN").val()
            }
        }, function(data){
                    if(data.status == 200){
                        $("#spinMessages").html('De reservering is geplaatst. Je ontvangt binnen enkele minuten een email met de bevestiging. Je kunt dit venster nu sluiten');
                        $("#spinMessages").append('<p>klik <a href="'+data.link+'">hier</a> als u niet automatisch wordt doorgestuurd.</p>');
                        window.location.href = data.link;
                    }
                    else{
                        alert('De reservering kon niet worden geplaatst. We kampen met een technische storing, of het formulier was onjuist ingevuld. Je kunt dit venster nu sluiten');
                    }
            }, 'json');
        });
    });
});

function gotoStep(step){
    $(".book-step").fadeOut('fast', function(){
        $("#step" + step).fadeIn();
    });
}

function convertPhone(input, plusSymbol){
    var plus = '';
    if(plusSymbol) plus = '+';
    var result = String(input).replace(/ /g,'');
    if(result.charAt(0) == '0') result = plus + '31' + input.substring(1);
    if(result.charAt(0) != "+"){
        result = plus + result;
    }
    return result;
}

function updateDateInputs(){
    // get the iso time string formatted for usage in an input['type="datetime-local"']
    var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
    var localISOTime = (new Date(Date.now() - tzoffset)).toISOString().slice(0,-1);
    var localISOTimeWithoutSeconds = localISOTime.slice(0,16);

    // select the "datetime-local" input to set the default value on
    var dtlInput = document.querySelector('input[type="datetime-local"]');

    // set it and forget it ;)
    dtlInput.value = localISOTime.slice(0,16);
}

function calculateRoutes(sLat, sLng){

    $.getJSON('/api/v2/rides/tesla-route?start='+sLat+','+sLng, function(data){
        console.log(data);
        $("#inputDuration").val(''+data.route.routes[0].legs[0].duration.value);
        $("#inputDistance").val(''+data.route.routes[0].legs[0].distance.value);
        $("#inputPrice").val(''+data.total);

        if(data.percentage > 0){
            $('#lblPsubtotal').html('<label>Ritprijs : </label> € '+data.price);
            $('#lblPdiscount').html('<label>Korting '+data.percentage+'% : </label> € '+data.discount);
        }else{
            $('#lblPsubtotal').html('');
            $('#lblPdiscount').html('');
        }
        $('#lblPgtotal').html('<label>Totaal : </label> € '+(data.total));

    });
}